f = open("day2.txt", "r")
file1 = f.readlines()
passwords = 0
for line in file1:
    index = 0
    match = line.rstrip()
    match = match.replace("-", ' ')
    match = match.replace(": ", ' ')
    match = match.split(' ')
    index = match[3].find(match[2])
    try:
        if(match[3][int(match[0])-1] == match[2] and match[3][int(match[1])-1] != match[2]):
            passwords = passwords + 1
        elif(match[3][int(match[1])-1] == match[2] and match[3][int(match[0])-1] != match[2]):
            passwords = passwords + 1
    except:
        continue
print(passwords)