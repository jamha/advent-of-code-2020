with open("day3.txt", "r") as f:
    next(f)
    next(f)
    tree = 0
    lineNo = 1
    for line in f.read().split("\n")[::2]:
        lineNo = lineNo + 1
        if(lineNo > 31):
            lineNo = lineNo - 31
        if(line[lineNo - 1] == '#'):
            tree = tree + 1

    print(tree)