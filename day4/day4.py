import ast, re

passports = 0
validKeys = {'byr','iyr','eyr','hgt','hcl','ecl','pid'}
validEyeColor = {'amb', 'blu','brn','gry', 'grn', 'hzl', 'oth'}

def validatePassport(dictionary):
    validateChecks = 0
    if(len(dictionary['byr']) == 4 and 1920 <= int(dictionary['byr']) <= 2002):
        validateChecks += 1
    if(len(dictionary['iyr']) == 4 and 2010 <= int(dictionary['iyr']) <= 2020):
        validateChecks += 1
    if(len(dictionary['eyr']) == 4 and 2020 <= int(dictionary['eyr']) <= 2030):
        validateChecks += 1
    ##check height
    height = re.split("(\d+)", dictionary['hgt'])
    
    if(height[2] == 'in' and 59 <= int(height[1]) <= 76):
        validateChecks += 1
    elif(height[2] == 'cm' and 150 <= int(height[1]) <= 193):
        validateChecks += 1

    if(dictionary['hcl'][0] == '#' and dictionary['hcl'][1:].isalnum()):
        validateChecks += 1
    if(dictionary['ecl'] in validEyeColor):
        validateChecks += 1
    if(len(dictionary['pid']) == 9):
        validateChecks += 1
    
    if(validateChecks == 7):
        return True
    else:
        return False

with open("day4.txt", "r") as f:
    for group in f.read().split('\n\n'):
        d = {}
        for attr in group.split():
            (key, val) = attr.split(':')
            d[key] = val
        if(validKeys <= d.keys() and validatePassport(d)):
                passports += 1 
        
print(passports)