
with open("day6.txt", "r") as f:
    count = 0
    for group in f.read().split('\n\n'):
        unique = []
        for char in group[::]:
            print(char)
            if char not in unique and char != '\n':
                unique.append(char)
        count += len(unique)
print(count)